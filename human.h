#ifndef HUMAN_H
#define HUMAN_H
#include "animal.h"


class Human : public Animal {
  public:
    Human();
    virtual ~Human();
    virtual void speak();
};

#endif // HUMAN_H
