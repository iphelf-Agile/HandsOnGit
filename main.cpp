#include <iostream>
#include "cat.h"
#include <dog.h>
#include "cow.h"
#include "human.h"

using namespace std;

int main() {
    cout<<"***** HandsOnGit *****"<<endl;

    Animal *animal;

    animal=new Dog();
    animal->speak();

    animal=new Cat;
    std::cout<<"cat:";
    animal->speak();

    animal=new Cow;
    animal->speak();

    animal=new Human;
    std::cout<<"Human:";
    animal->speak();
    return 0;
}
