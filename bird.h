#ifndef BIRD_H
#define BIRD_H
#include "animal.h"


class Bird : public Animal {
  public:
    Bird();
    virtual ~Bird();
    virtual void speak();
};

#endif // BIRD_H
