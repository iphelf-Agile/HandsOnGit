TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        animal.cpp \
        bird.cpp \
        cat.cpp \
        dog.cpp \
        cow.cpp \
        human.cpp \
        main.cpp

HEADERS += \
    animal.h \
    bird.h \
    cat.h \
    cow.h \
    dog.h \
    cow.h \
    human.h
