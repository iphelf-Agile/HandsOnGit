#ifndef ANIMAL_H
#define ANIMAL_H


class Animal {
  public:
    Animal();
    virtual ~Animal();
    virtual void speak();
};

#endif // ANIMAL_H
