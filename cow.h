#ifndef COW_H
#define COW_H
#include "animal.h"

class Cow:public Animal {
  public:
    Cow();
    virtual ~Cow();
    virtual void speak();
};

#endif // COW_H
